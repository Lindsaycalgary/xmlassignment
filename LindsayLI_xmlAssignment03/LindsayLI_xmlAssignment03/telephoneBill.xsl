<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
		<!-- target the root element of the xml file to start looking for data-->
		<!-- Nicely done here, Lindsay.  Your xsl properly formats and outputs the
xml data and you have a comment.  Good work
10/10
-->
		<html>
			<head>
				<title>telephoneBill</title>
			</head>
			<body>
				<table>
					<tbody>
						<tr>
							<th colspan="2">Customer Info</th>
						</tr>
						<tr>
							<td>
							Name:
							<xsl:value-of select="telephoneBill/customer/name"/>
							</td>
						</tr>
						<tr>
							<td>
							Address:
							<xsl:value-of select="telephoneBill/customer/address"/>
							</td>
						</tr>
						<tr>
							<td>
							City:
							<xsl:value-of select="telephoneBill/customer/city"/>
							</td>
						</tr>
						<tr>
							<td>
							Province:
							<xsl:value-of select="telephoneBill/customer/province"/>
							</td>
						</tr>
					</tbody>
				</table>
				<table border="1">
					<tbody>
						<tr>
							<th>Called Number</th>
							<th>Date</th>
							<th>Duration In Minutes</th>
							<th>Charge</th>
						</tr>
						<!-- here I'm going to start looping -->
						<xsl:for-each select="telephoneBill/call">													
							<tr>
								<td>
									<xsl:value-of select="@number"/>
								</td>
								<td>
									<xsl:value-of select="@date"/>								
								</td>
								<td>
									<xsl:value-of select="@durationInMinutes"/>
								</td>
								<td>
									<xsl:value-of select="@charge"/>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>		
		</html>
	</xsl:template>
</xsl:stylesheet>
